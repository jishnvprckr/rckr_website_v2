$(document).ready(function () {
  $(".menu-button").on("click", function () {
    $("body").toggleClass("showMenu");
  });
});

var position = $(window).scrollTop();
var headerHeight = 70;
$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll > position) {
    $("body").removeClass("header-popup-show");
    $("body").removeClass("normal-header");
    if (scroll > headerHeight) {
      $("body").addClass("header-popup");
      $("body").addClass("hide-header-popup");
    } else {
    }
  } else {
    if (scroll > 0) {
      $("body").addClass("header-popup-show");
    } else {
      $("body").removeClass("header-popup");
      $("body").removeClass("header-popup-show");
      $("body").addClass("normal-header");
    }
  }
  position = scroll;
});

function codeAddress() {
  var loc = location.pathname;
  var locNavs = document.getElementsByClassName("nav-link");
  for (var i = 0; i < locNavs.length; i++) {
    if (locNavs[i].getElementsByTagName('a')[0].getAttribute("href") == loc) {
      locNavs[i].className += " active";
    }
  }
}

window.onload = codeAddress;
